package kr.co.hbilab.board;

public class BoardVO {
	private int bno;
	private String title;
	
	public BoardVO(int bno, String title) {
		super();
		this.bno = bno;
		this.title = title;
	}
	
	
	public int getBno() {
		return bno;
	}
	public void setBno(int bno) {
		this.bno = bno;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	
}
